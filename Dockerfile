FROM python:3.9.5-slim

COPY ./requirements.txt /app/requirements.txt

RUN cd /app/ && \
    pip install -U pip && \
    pip install -r requirements.txt

ENV FLASK_APP app.py
ENV FLASK_ENV development

COPY src /app/src

WORKDIR /app/src

CMD ["flask", "run", "--host=0.0.0.0", "--port=5000", "--reload"]
